# Tutorial 2
*Nama: Ahmad Haydar Alkaf*
*NPM: 1906306640*
#### Scene `BlueShip` dan `StonePlatform` sama-sama memiliki sebuah _child node_ bertipe `Sprite`. Apa fungsi dari _node_ bertipe `Sprite`?
- `Sprite` merupakan node yang menampilkan `2D Texture`. Bisa berupa sebuah frame _spritesheet_(Setelah di sesuaikan `hframe`, `vframe`, `frame` nya) atau `atlas Texture`(I don't know what is that exactly, tidak pernah memakai istilah tersebut saat membuat game saya). Selain mendisplay sebuah _frame spritesheet_, Bisa menggunakan `animationplayer/animationtreeplayer` untuk menganimasikannya dengan menggunakan _value_ dari `frame` spritesheet.

#### _Root node_ dari _scene_ `BlueShip` dan `StonePlatform` menggunakan tipe yang berbeda. `BlueShip` menggunakan tipe `RigidBody2D`, sedangkan `StonePlatform` menggunakan tipe `StaticBody2D`. Apa perbedaan dari masing-masing tipe _node_?
- `StaticBody2D` merupakan `body` yang tidak dimaksud untuk digerakkan. `body` yang ini berguna untuk diimplementasikan sebagai tembok atau objek environment lain. Sedangkan `RigidBody2D` merupakan node yang mengimplementasikan simulasi _physics_, kita tidak dapat mengontrol `RigidBody2D` secara langsung seperti `KinematicBody2D` yang dapat langsung diubah velocity vectornya. `RigidBody2D` dapat digerakkan dengan method-method seperti `add_force`, dll yang mempengaruhi gaya.

#### Ubah nilai atribut `Mass` dan `Weight` pada tipe `RigidBody2D` secara bebas di _scene_ `BlueShip`, lalu coba jalankan _scene_ `Main`. Apa yang terjadi?
- Sebenarnya tidak ada hal notable yang terjadi. Mau seperti apapun gravitasi memiliki percepatan tetap, mau seberat apapun semua akan jatuh dengan kecepatan yang sama dalam ruang vakum, cuma pergantian arah gaya akan lebih susah dipengaruhi.

#### Ubah nilai atribut `Disabled` pada tipe `CollisionShape2D` di _scene_ `StonePlatform`, lalu coba jalankan _scene_ `Main`. Apa yang terjadi?
- _Swwwwingggg!!_ ya jelas jatuh `BlueShip`nya, karena `CollisionShape2D` di disable pastinya tidak akan ada collision yang terjadi antara kedua objek sehingga... ya begitulah.

####  Pada _scene_ `Main`, coba manipulasi atribut `Position`, `Rotation`, dan `Scale` milik _node_ `BlueShip` secara bebas. Apa yang terjadi pada visualisasi `BlueShip` di Viewport?
- Posisi inisial `BlueShip` berubah, Arah `BlueShip` Juga berubah, Besarnya `BlueShip` Juga Berubah.

#### Pada _scene_ `Main`, perhatikan nilai atribut `Position` _node_ `PlatformBlue`, `StonePlatform`, dan `StonePlatform2`. Mengapa nilai `Position` _node_ `StonePlatform` dan `StonePlatform2` tidak sesuai dengan posisinya di dalam _scene_ (menurut Inspector) namun visualisasinya berada di posisi yang tepat?
- Posisi suatu _node_ di godot relatif terhadap _node_ parentnya. As simple as that.
